EXECUTABLE=pnda-cloud
WINDOWS=$(EXECUTABLE)_windows_amd64.exe
LINUX=$(EXECUTABLE)_linux_amd64
DARWIN=$(EXECUTABLE)_darwin_amd64
VERSION=$(shell cat version.txt)
COMMIT_HASH=$(shell git rev-parse HEAD)
BUILD_TIME=$(shell date +%s)

GO_ARGS=-a -mod=vendor -ldflags "-s -w -X main.sha1Version=$(COMMIT_HASH) -X main.buildTime=$(BUILD_TIME)"

.PHONY: all clean

all: build

update-version:
	@echo "Latest version found: $(git describe --tags --abbrev=0)"
	@echo "Version in version.txt: $(cat version.txt)"
	@read -p "New Version: " newVersion
	@echo "Writing ${newVersion} into version.txt"
	@echo -n ${newVersion} > version.txt
	@echo "Creating commit 'chore: update version to ${newVersion}' and push"
	@git stage version.txt
	@git commit -m "chore: update version to ${newVersion}"
	@git push -u
	@echo "Create tag correcponding to that version, this will trigger the release once pushed"
	@git tag ${newVersion}
	@git push --tags

build: windows linux darwin ## Build binaries
	@echo version: $(VERSION)

windows: $(WINDOWS) ## Build for Windows

linux: $(LINUX) ## Build for Linux

darwin: $(DARWIN) ## Build for Darwin (macOS)

$(WINDOWS):
	@echo -e "\e[1;35m \t Building windows-amd64 binary \e[1;m"
	env GOOS=windows GOARCH=amd64 go build -o ./bin/$(WINDOWS) $(GO_ARGS) ./main.go

$(LINUX):
	@echo -e "\e[1;35m \t Building linux-amd64 binary \e[1;m"
	env GOOS=linux GOARCH=amd64 go build -o ./bin/$(LINUX) $(GO_ARGS) ./main.go

$(DARWIN):
	@echo -e "\e[1;35m \t Building darwin-amd64 binary \e[1;m"
	env GOOS=darwin GOARCH=amd64 go build -o ./bin/$(DARWIN) $(GO_ARGS) ./main.go

clean: ## Remove previous build
	rm -f $(WINDOWS) $(LINUX) $(DARWIN)

help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
