package console

import (
	"github.com/pterm/pterm"
)

func startUI() {
	s, _ := pterm.DefaultBigText.WithLetters(
		pterm.NewLettersFromStringWithStyle("PNDA Cloud", pterm.NewStyle(pterm.FgGreen))).Srender()
	pterm.DefaultCenter.Println(s)

	// area, _ := pterm.DefaultArea.WithCenter().Start()
	// for i := 0; i < 10; i++ {
	// 	str, _ := pterm.DefaultBigText.WithLetters(pterm.NewLettersFromString(time.Now().Format("15:04:05"))).Srender()
	// 	area.Update(str)
	// 	time.Sleep(time.Second)
	// }
	// area.Stop()
}
