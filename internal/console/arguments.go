package console

import (
	"flag"
	"fmt"

	"github.com/pterm/pterm"
)

func ReadCliArguments() {
	rawOutputPtr := flag.Bool("raw-output", false, "Disable styled output")

	updatePrt := flag.Bool("update", false, "Update to latest version")
	restartPtr := flag.Bool("restart", false, "Restart the service")
	versionPtr := flag.Bool("version", false, "Display versions of different services")
	verifyPtr := flag.Bool("verify", false, "Verify validity of local copy of PNDA")
	uiPtr := flag.Bool("ui", false, "Show UI")

	flag.Parse()

	if *rawOutputPtr == true {
		pterm.DisableStyling()
	}

	if *updatePrt == true {
		fmt.Println("Update")
	}

	if *restartPtr == true {
		fmt.Println("Restart")
	}

	if *versionPtr == true {
		fmt.Println("Version")
	}

	if *verifyPtr == true {
		fmt.Println("Verify")
	}

	if *uiPtr == true {
		fmt.Println("UI")
		startUI()
	}

	fmt.Println("tail:", flag.Args())
}
